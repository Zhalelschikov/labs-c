#include <stdio.h>

int main()
{
	int height;
	char dimension;
	float result;
	printf ("Enter your height (f or d (example: 6f or 74d)), please:\n");
	scanf ("%d",&height);
	scanf ("%c",&dimension);
	printf ("Your height is: %d (%c) \n",height,dimension);
	if (dimension=='f'){result=(height*2.54*12);printf ("Result is: %4.1f (cm)\n",result);}
	if (dimension=='d'){result=(height*2.54);printf ("Result is: %4.1f (cm)\n",result);}

return 0;
}