#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	int number,EnteredNumber;
	srand (time(0));
	number = rand() % 100 + 1; 
	//printf("number is: %d\n", number);
	printf ("Guess a number from 1 to 100, please.\n\n");
	scanf ("%d",&EnteredNumber);
	printf ("You entered a number: %d\n\n",EnteredNumber);
	
	while (EnteredNumber!=number)
	{
		if (EnteredNumber>number)
		{
			printf ("No, you did not guess the hidden number is less, try again, please.\n\n");
			printf ("Guess a number from 1 to 100, please.\n\n");
			scanf ("%d",&EnteredNumber);
			printf ("You entered a number: %d\n\n",EnteredNumber);
		}
		if (EnteredNumber<number)
		{
			printf ("No, you did not guess the hidden number is more, try again, please.\n\n");
			printf ("Guess a number from 1 to 100, please.\n\n");
			scanf ("%d",&EnteredNumber);
			printf ("You entered a number: %d\n\n",EnteredNumber);
		}
	}
	
	printf ("Yes you guessed!!!\n\n");
return 0;
}