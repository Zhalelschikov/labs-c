#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
	unsigned char buf[256];
	int counts[256]={0},count=0,i=0;
	puts("Enter a string: ");
	fgets(buf,256,stdin);
	buf[strlen(buf)-1]=0;

	while(buf[i])
	{
		counts[buf[i]]++;
		i++;
	}
	count=i; i=0;
	while(i<256)
	{
		if (counts[i])
			printf("%c-%d\n",i,counts[i]);
		i++;
	}
	
return 0;
}