#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define N 10

char randoms()
{
	switch (rand()%2)
	{
	case 0:
		return rand()%('9'-'0'+1)+'0';
		break;
	case 1:
		return rand()% 10 + (-10);
		break;
	}
}

int main()
{
	int arr[N];
	int i=0,sum=0,n=0,p=0;
	srand(time(0));
	for(i=0;i<N;i++)
	{
		
			arr[i]=randoms();
			printf("%02d ",arr[i]);
				
	}
	putchar('\n');
	
    for(i=0;i<N;i++)
	{
		if(arr[i]<n)
		    n=arr[i];
    }
	
    for(i=0;i<N;i++)
	{
		if(arr[i]>p)
		    p=arr[i];
    }
	
    sum=n+p;
	
    printf("Negative number = %d\nPositive number = %d\nSum = %d\n",n,p,sum);
   
	return 0;
}