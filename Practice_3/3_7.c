#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
	unsigned char buf[256];
	int counts[256]={0},i=0,j=0,max=0,m=0,n=0;
	puts("Enter a string: ");
	fgets(buf,256,stdin);
	buf[strlen(buf)-1]=0;

	while(buf[i])
	    {
		    counts[buf[i]]++;
		    i++;
	    }
	
    i=0;
    
    while(i<256)
	{
		if (counts[i])
			n++;
		i++;
	}

    i=0;
    
    while (j<=n-1)
        {
            while(i<256)
	            {
		            if(counts[i]>max)
                        {
                            max=counts[i];
                            m=i;
                        }
        
		            else i++;
	            }
    
            printf("%c-%d\n",m,max);
            counts[m]=0;
            max=0;
            i=0;
            j++;
        }
  
return 0;
}