#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void swap(char *p,char *q)
{
    char t;
    while(p<q)
    {
        t=*p;
        *p=*q;
        *q=t;
        p++;
        q--;
    }
}

void chomp(char *buf)
{
    if(buf[strlen(buf)-1]=='\n')
        buf[strlen(buf)-1]=0;
}

int main ()
{
    char buf[80],*beg,*end;
    int i=0, flag=0;
    puts("Enter a string");
    fgets(buf,80,stdin);
    chomp(buf);
    while(buf[i])
    {
        if(buf[i]!=' ' && flag==0)
        {
            flag=1;
            beg=buf+i;
        }
        else if(buf[i]==' ' && flag==1)
        {
            flag=0;
            end=buf+i-1;
            swap(beg,end);
        }
        i++;
    }
    if(flag==1)
        swap(beg,buf+i-1);
    puts(buf);

return 0;
}